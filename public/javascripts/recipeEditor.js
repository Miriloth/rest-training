/**
 * Created by michal on 11.02.16.
 */
$(document).ready(function () {
    populateTable();
    $('#recipeList').on('click', 'li a.recipeDescription', showUserInfo);
    $('#addRecipeBtn').on('click', addRecipe);
    $('#recipeList').on('click', 'li a.recipeDeleteBtn', deleteUser)
});

var recipeListData = [];

function populateTable() {
    var tableContent = '';

    $.getJSON('/recipe/recipelist', function (data) {

        recipeListData = data;
        $.each(data, function () {
            tableContent += '<li>';
            tableContent += '<h3>Name: <a href="#" class="recipeDescription" rel="' + this.name + '">' + this.name + '</a>';
            tableContent += ' Time: ' + this.time;
            tableContent += '<a class="recipeDeleteBtn" rel=' + this._id + '><span class="glyphicon glyphicon-remove pull-right"></span></a></h3></li>';
        });
        $('#recipeList').html(tableContent);
        console.log(tableContent);
    });
}
function showUserInfo(event) {
    event.preventDefault();

    var thisRecipeName = this.rel;

    var arrayPosition = recipeListData.map(function (arrayItem) {
        return arrayItem.name
    }).indexOf(thisRecipeName);

    var thisRecipeObject = recipeListData[arrayPosition];

    $('#name').text(thisRecipeObject.name);
    $('#description').text(thisRecipeObject.description);
}

function addRecipe(event) {
    event.preventDefault();

    var errorCount = 0;
    $('#newRecipe input').each(function (index, val) {
        if ($(this).val() === '') {
            errorCount++;
        }
    });

    if (errorCount === 0) {
        var newRecipe = {
            "name": $('#newRecipe input#inputRecipeName').val(),
            "description": $('#newRecipe input#inputRecipeDescription').val(),
            "time": $('#newRecipe input#inputRecipeTime').val()
        };

        $.ajax({
            type: 'POST',
            data: newRecipe,
            url: '/recipe/addrecipe',
            dataType: 'JSON'
        }).done(function (response) {
            if (response.msg === '') {
                $('#newRecipe input').val('');
                populateTable();
            }
            else {
                alert('Error: ' + response.msg)
            }
        })
    }
    else {
        alert('Fill all the fields.');
        return false;
    }
}

function deleteUser(event) {
    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this recipe?');

    if (confirmation === true) {
        $.ajax({
            type: 'DELETE',
            url: '/recipe/deleterecipe/' + $(this).attr('rel')
        }).done(function (response) {
            if (response.msg === '') {
            }
            else {
                alert('Error' + response.msg);
            }
            populateTable();
        })
    }
    else {
        return false;
    }
}

