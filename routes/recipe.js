var express = require('express');
var router = express.Router();

router.get('/recipelist', function (req, res, next) {
    var db = req.db;
    var collection = db.get('recipe');
    collection.find({}, {}, function (e, docs) {
        res.json(docs);
    })
});

router.post('/addrecipe', function(req, res) {
    var db = req.db;
    var collection = db.get('recipe');
    collection.insert(req.body, function(err, result){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
    });
});

router.delete('/deleterecipe/:id', function(req, res){
    var db = req.db;
    var collection = db.get('recipe');
    var recipeToDelete = req.params.id;
    collection.remove({'_id' : recipeToDelete}, function(err){
        res.send((err === null) ? {msg: ''} : {msg:'error' + error});
    })
});

module.exports = router;
